import { init } from 'snabbdom/init'
import { classModule } from 'snabbdom/modules/class'
import { propsModule } from 'snabbdom/modules/props'
import { styleModule } from 'snabbdom/modules/style'
import { eventListenersModule } from 'snabbdom/modules/eventlisteners'
import { h } from 'snabbdom/h' // helper function for creating vnodes
import { toVNode } from 'snabbdom/tovnode'

// 创建出patch 函数 
var patch = init([ // Init patch function with chosen modules
    classModule, // makes it easy to toggle classes
    propsModule, // for setting properties on DOM elements
    styleModule, // handles styling on elements with support for animations
    eventListenersModule, // attaches event listeners
])



// 得到按钮和container
const btn = document.querySelector('.btn');
const container = document.querySelector('.container')

// 创建虚拟节点
const vnode1 = h('div', {}, [
    h('p', { key: "A" }, 'A'),
    h('p', { key: "B" }, 'B'),
    h('p', { key: "C" }, 'C'),
    h('p', { key: "D" }, 'D'),
])
console.log(vnode1)

// patch 让虚拟节点上树
patch(container, vnode1);
const vnode2 = h('ul', {}, [
        h('p', { key: "E" }, 'E'),
        h('p', { key: "A" }, 'A'),
        h('p', { key: "B" }, 'B'),
        h('p', { key: "C" }, 'C'),
        h('p', { key: "D" }, 'D'),

    ])
    // 点击按钮时将vnode1 变成vnode2

console.log(btn)
btn.onclick = function() {
    patch(vnode1, vnode2);
}