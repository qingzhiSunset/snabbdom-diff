import { init } from 'snabbdom/init'
import { classModule } from 'snabbdom/modules/class'
import { propsModule } from 'snabbdom/modules/props'
import { styleModule } from 'snabbdom/modules/style'
import { eventListenersModule } from 'snabbdom/modules/eventlisteners'
import { h } from 'snabbdom/h' // helper function for creating vnodes
import { toVNode } from 'snabbdom/tovnode'

// 创建出patch 函数 
var patch = init([ // Init patch function with chosen modules
    classModule, // makes it easy to toggle classes
    propsModule, // for setting properties on DOM elements
    styleModule, // handles styling on elements with support for animations
    eventListenersModule, // attaches event listeners
])

// 创建虚拟节点
var newVNode = h('div', { style: { color: '#000' } }, [
    h('h1', 'Headline'),
    h('p', 'A paragraph'),
    h('div', '我是div', [
        h('p', "我是div子元素p"),
        h('p', h('p', 'p元素的子元素'))
    ]),
])
console.log(newVNode)
console.log(document.querySelector('.container'))
    // patch 让虚拟节点上树
patch(document.querySelector('.container'), newVNode)