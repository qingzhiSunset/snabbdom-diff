import h from './src/MySnabbdom/h'
console.log(h('div', {}, "文字"));
console.log(h('ul', {}, [h('li', {}, "1"), h('p', {}, "2")]));
console.log(h('ul', {}, [h('li', {}, "1"), h('p', {}, [h('li', {}, "1"), h('p', {}, "2")])]));
console.log(h('p', {}, h('p', {}, "p>p")));