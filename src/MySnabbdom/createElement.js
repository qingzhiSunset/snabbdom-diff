//  真正的创建节点。将vnode 创建dom ，是孤儿节点不进行插入

export default function createElement(vnode) {

    // 创建节点
    let domNode = document.createElement(vnode.sel);

    // 有子节点还是文本
    if (vnode.text !== '' && (vnode.children === undefined || vnode.children.length === 0)) {
        //它内部是文字
        domNode.innerText = vnode.text;

    } else if (Array.isArray(vnode.children) && vnode.children.length > 0) {
        // 它内部是子节点
        // 递归创建节点
        for (let i in vnode.children) {
            // 得到当前的children
            let ch = vnode.children[i];
            /*  
              创建出他的DOM ,一旦调用createElement 意味着: 创建出了DOM节点, 
             并且它的elm 属性指向了它创建出的DOM, 但是还没有上树,是一个孤儿节点 
             
             */
            let chDOM = createElement(ch);
            // 子元素上树
            domNode.appendChild(chDOM);
        }
    }
    // 补充elm属性
    vnode.elm = domNode;

    // 返回一个elm ， elm 属性是一个纯DOM对象
    return vnode.elm
}