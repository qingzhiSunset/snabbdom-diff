import vnode from './vnode'

// 编写一个简单版本的h函数
/* 
    编写一个低版本的h函数， 这函数接收三个参数，缺一不可，
    相当于他的功能重载功能较弱
    也就是说该函数，调用的时候是以下三种情况中的一种
        形态一： h('div',{},"文字")
        形态二： h('div',{},[])
        形态三： h('div',{},h())



*/
export default function(sel, data, c) {
    // console.log(vnode(sel, data, c, 4))
    //检查参数的个数 
    if (arguments.length !== 3) {
        throw new Error("对不起，我是一个简单的 h函数 我必须传入3个参数")
    }
    // 检查参数c的类型

    if (typeof c === 'string' || typeof c === 'number') {
        // 说明h函数调用形态1 ;
        return vnode(sel, data, undefined, c, undefined)
    } else if (Array.isArray(c)) {

        // 说明在调用形态2

        // 收集children
        let children = [];
        for (let i = 0; i < c.length; i++) {
            // 判断数组参数是否符合条件
            if (!(typeof c[i] === 'object' && c[i].hasOwnProperty('sel'))) {
                throw new Error("传入的数组元素不正确")
            }
            // 此时 h函数已经执行完了
            // 只需要收集结果
            children.push(c[i]);
        }
        // 循环结束了。说明children收集完毕了
        return vnode(sel, data, children, undefined, undefined)
    } else if (typeof c === 'object' && c.hasOwnProperty('sel')) {
        //   说明调用形态三
        let children = [c];
        return vnode(sel, data, children, undefined, undefined)

    } else {
        throw new Error("传入第三个参数类型不对")
    }
}
// console.log(vnode('div', 1, 2, 3, 4))