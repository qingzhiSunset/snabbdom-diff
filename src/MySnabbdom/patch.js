import vnode from './vnode'
import createElement from './createElement'
import patchVnode from "./patchVnode"
export default function patch(oldVnode, newVnode) {
    // 判断第一个参数是虚拟节点，还是DOM节点
    if (oldVnode.sel === '' || oldVnode.sel === undefined) {
        // 说明是DOM节点
        // 此时需要包装虚拟节点
        oldVnode = vnode(oldVnode.tagName.toLowerCase(), {}, [], undefined, oldVnode);

    }

    // 判断oldVnode 和 newVnode是不是同一个节点
    if (oldVnode.key === newVnode.key && oldVnode.sel === newVnode.sel) {

        patchVnode(oldVnode, newVnode)

    } else {
        // 不是同一个节点 ，暴力删除
        console.log("不是同一个节点 ，暴力删除,插入新节点")
        let newNodeElm = createElement(newVnode);

        // 插入到老节点之前
        if (oldVnode.elm.parentNode && newNodeElm) {
            oldVnode.elm.parentNode.insertBefore(newNodeElm, oldVnode.elm);
        }

        // 删除老节点
        oldVnode.elm.parentNode.removeChild(oldVnode.elm);
    }

}