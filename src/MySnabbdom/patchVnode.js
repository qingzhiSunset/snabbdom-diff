import createElement from './createElement'
import updateChildren from './updateChildren'
export default function patchVnode(oldVnode, newVnode) {
    // 说明书是同一个节点
    // 判断新旧节点是否是同一个对象
    if (oldVnode === newVnode) return;

    // 判断newVnode有没有text属性，
    if (newVnode.text !== undefined && (newVnode.children === undefined || newVnode.children.length === 0)) {
        // 新节点有text属性
        console.log("newVnode 有text属性")
            // 新老节点text属性有text属性
        if (oldVnode.text !== newVnode.text) {
            // 新老节点text不相同时，修改text值
            oldVnode.elm.innerText = newVnode.text;

        }
    } else {
        //新节点没有text属性
        // 判断老节点有没有children节点
        if (oldVnode.children !== undefined && oldVnode.children.length > 0) {

            //  oldVnode有children 并且newVnode也有children
            updateChildren(oldVnode.elm, oldVnode.children, newVnode.children)

        } else {

            // 清空老节点的内容
            oldVnode.elm.innerText = '';

            // oldVnode没有children 新的节点有children

            for (let childNode of newVnode.children) {
                console.log(childNode)
                    //创建dom节点
                let dom = createElement(childNode);
                // 老节点上树
                oldVnode.elm.appendChild(dom)
            }

        }

    }
}