import createElement from './createElement'
export default function patchVnode(oldVnode, newVnode) {
    // 说明书是同一个节点
    // 判断新旧节点是否是同一个对象
    if (oldVnode === newVnode) return;

    // 判断newVnode有没有text属性，
    if (newVnode.text !== undefined && (newVnode.children === undefined || newVnode.children.length === 0)) {
        // 新节点有text属性
        console.log("newVnode 有text属性")
            // 新老节点text属性有text属性
        if (oldVnode.text !== newVnode.text) {
            // 新老节点text不相同时，修改text值
            oldVnode.elm.innerText = newVnode.text;

        }
    } else {
        //新节点没有text属性
        // 判断老节点有没有children节点
        if (oldVnode.children !== undefined && oldVnode.children.length > 0) {

            //  所有未处理节点的开头
            let un = 0;

            // oldVnode 有children 此时就是最复杂情况， 就是新老节点都有children属性

            for (let i in newVnode.children) {
                let newVnodeChild = newVnode.children[i];
                // console.log(vnodeChiild)
                let isExist = false;
                for (let oldVnodeChild of oldVnode.children) {
                    // 判断是否有该节点
                    if (oldVnodeChild.sel === newVnodeChild.sel && oldVnodeChild.key === newVnodeChild.key) {
                        isExist = true;
                    }
                }

                if (!isExist) {
                    // 说明oldVnode 中没有此节点
                    let dom = createElement(newVnodeChild);
                    newVnodeChild.elm = dom;
                    //  插入到未处理之前节点之前
                    if (un < oldVnode.children.length) {
                        oldVnode.elm.insertBefore(dom, oldVnode.children[un].elm);
                    } else {
                        oldVnode.elm.appendChild(dom)
                    }


                    console.log(newVnodeChild)

                } else {
                    // 让处理的指针节点下移
                    un++;
                }

            }

        } else {

            // 清空老节点的内容
            oldVnode.elm.innerText = '';

            // oldVnode没有children 新的节点有children

            for (let childNode of newVnode.children) {
                console.log(childNode)
                    //创建dom节点
                let dom = createElement(childNode);
                // 老节点上树
                oldVnode.elm.appendChild(dom)
            }

        }

    }
}