import patchVnode from './patchVnode'
import createElement from './createElement'
// 判断是否是同一个虚拟节点
function checkSameVnode(a, b) {
    return a.sel === b.sel && a.key === b.key;
}

export default function updateChildren(parentElm, oldChildren, newChildren) {

    console.log("updateChildren")
        // console.log(parentElm, oldChildren, newChildren)

    /* ---指针----*/

    // 旧前 ----> 指向旧节点第一个
    let oldStartIndex = 0;
    // 新前 ----> 指向新节点第一个
    let newStartIndex = 0;
    // 新后  ----> 指向新节点最后一个
    let newEndIndex = newChildren.length - 1;
    // 旧后  ----> 指向旧节点最后一个
    let oldEndIndex = oldChildren.length - 1;


    /* ------节点------- */

    // 旧前节点
    let oldStartVnode = oldChildren[oldStartIndex];
    // 旧后节点
    let oldEndVnode = oldChildren[oldEndIndex];
    // 新前节点
    let newStartVnode = newChildren[newStartIndex];
    // 新后节点
    let newEndVnode = newChildren[newEndIndex];


    // keymap 缓存
    let keyMap = null;

    while (oldStartIndex <= oldEndIndex && newStartIndex <= newEndIndex) {
        // console.log(newEndVnode, oldEndVnode)
        console.log(oldStartVnode, oldStartIndex, oldEndVnode, oldEndIndex, newStartVnode, newStartIndex, newEndVnode, newEndIndex)
            // 首先不是判断是否命中策略一、二、三、四 ，而是先判断是否略过 undefined标记的东西

        if (oldStartVnode == null) {
            oldStartVnode = oldChildren[++oldStartIndex]
        } else if (oldEndVnode == null) {
            oldEndVnode = oldChildren[--oldEndIndex]
        } else if (newStartVnode == null) {
            newStartVnode = newChildren[++newStartIndex]
        } else if (oldEndVnode == null) {
            newEndVnode = newChildren[--newEndIndex]
        }
        // 策略一： 旧前与新前 相同
        else if (checkSameVnode(oldStartVnode, newStartVnode)) {
            console.log("策略一： 旧前与新前 相同")
                // 判断节点内容，更新里面的内容
            patchVnode(oldStartVnode, newStartVnode);
            // 更新指针，oldStartIndex 、 newStartVnode 都后移
            oldStartVnode = oldChildren[++oldStartIndex];
            newStartVnode = newChildren[++newStartIndex];
            console.log("策略一成功")

        } else if (checkSameVnode(newEndVnode, oldEndVnode)) {

            // 策略二： 旧后与新后 相同
            console.log("策略二： 旧后与新后 相同")

            // 判断节点内容，更新里面的内容
            patchVnode(oldEndVnode, newEndVnode);
            // 更新指针，oldEndIndex 前移 、 newEndIndex 前移
            oldEndVnode = oldChildren[--oldEndIndex];
            newEndVnode = newChildren[--newEndIndex];
            console.log("策略二成功")
        } else if (checkSameVnode(oldStartVnode, newEndVnode)) {
            // 策略三： 旧前与新后 相同  将新节点放在旧后之后
            console.log(" 策略三： 旧前与新后 相同  将新节点放在旧后之后")
                // 判断节点内容，更新里面的内容
            patchVnode(oldStartVnode, newEndVnode);

            // 将新节点放在旧后之后
            // 如何移动节点 ？？ 只要插入一个已经在dom树上的节点，他就会移动
            /* 
             Node.insertBefore()
                Node.nextSibling 是一个只读属性，返回其父节点的 childNodes 列表中紧跟在其后面的节点，如果指定的节点为最后一个节点，则返回 null。
                没有 insertAfter()。不过，可以使用 insertBefore 和 Node.nextSibling 来模拟它。
                在前一个例子中，可使用下面代码将 sp1 插入到 sp2 之后：
                        parentDiv.insertBefore(sp1, sp2.nextSibling);

                如果 sp2 没有下一个节点，则它肯定是最后一个节点，则 sp2.nextSibling 返回 null，且 sp1 被插入到子节点列表的最后面（即 sp2 后面）。
            
            */
            parentElm.insertBefore(oldStartVnode.elm, oldEndVnode.elm.nextSibling)
                // 更新指针，oldStartIndex后移 、 newEndIndex 前移
            oldStartVnode = oldChildren[++oldStartIndex];
            newEndVnode = newChildren[--newEndIndex];
            console.log("策略三成功")
        } else if (checkSameVnode(oldEndVnode, newStartVnode)) {
            // 策略四： 旧后与新前 相同  将新前指向节点放在旧前之前
            console.log(" 策略四： 旧后与新前 相同  将新节点放在旧后之后")
                // 判断节点内容，更新里面的内容
            patchVnode(oldEndVnode, newStartVnode);

            // 如何移动节点 ？？ 只要插入一个已经在dom树上的节点，他就会移动
            // 因为新前节点与旧后节点相同所以将旧后几点移动到旧前节点之前
            parentElm.insertBefore(oldEndVnode.elm, oldStartVnode.elm)

            // 更新指针，oldEndIndex前移 、 newStartIndex 后移
            oldEndVnode = oldChildren[--oldEndIndex];
            newStartVnode = newChildren[++newStartIndex];
            console.log("策略四成功")
        } else {
            // 四种都么有命中
            console.log(5, "四种都没命中")
                // 储存key,制作keyMap映射，之后不需要每次都遍历
            if (!keyMap) {
                keyMap = {}
                for (let i = oldStartIndex; i < oldEndIndex; i++) {
                    const key = oldChildren[i] === undefined ? undefined : oldChildren[i].key;
                    if (key !== undefined) {
                        keyMap[key] = i
                    }
                }
            }

            // 寻找到当前项（newStartIndex） 这项在keyMap 中映射的位置
            const indexOld = keyMap[newStartVnode.key];

            // 判断。如果indexOld 是undefined 表示是全新的项
            if (indexOld == undefined) {
                console.log("全新项");
                // 全新项
                parentElm.insertBefore(createElement(newStartVnode), oldStartVnode.elm)
                console.log("全新项成功");
            } else {

                console.log("移动项");
                // 不是全新的项,需要移动
                const elmToMove = oldChildren[indexOld];
                console.log(elmToMove)
                patchVnode(elmToMove, newStartVnode);
                // 把这项设置成undefined
                oldChildren[indexOld] = undefined;

                // 调用insertBefore 移动 到 旧节点开始之前
                parentElm.insertBefore(elmToMove.elm, oldStartVnode.elm)
                console.log("移动项成功");
            }

            //  移动新的头节点
            newStartVnode = newChildren[++newStartIndex];
            console.log("四个未命中成功")

        }
    }

    // 是否有剩余节点
    console.log("有剩余开始");
    // 循环结束之后，如果 旧节点的开始指针 小于 旧节点的尾指针 ==> 说明有节点剩余  要删除旧节点
    if (oldStartIndex <= oldEndIndex) {
        // 旧节点要删除
        // 批量删除旧节点剩余节点
        for (let i = oldStartIndex; i <= oldEndIndex; i++) {
            if (oldChildren[i]) {
                parentElm.removeChild(oldChildren[i].elm)
            }

        }
    }
    // 循环结束之后，如果 新节点的开始指针 小于 新节点的尾指针 ==> 说明新节点有节点剩余  旧节点中要新增节点
    else if (newStartIndex <= newEndIndex) {

        // 插入节点的标杆
        const before = newChildren[newEndIndex + 1] == null ? null : newChildren[newEndIndex + 1].elm;
        for (let i = newStartIndex; i <= newEndIndex; i++) {
            /* 
                Node.insertBefore()
                Node.nextSibling 是一个只读属性，返回其父节点的 childNodes 列表中紧跟在其后面的节点，如果指定的节点为最后一个节点，则返回 null。
                没有 insertAfter()。不过，可以使用 insertBefore 和 Node.nextSibling 来模拟它。
                在前一个例子中，可使用下面代码将 sp1 插入到 sp2 之后：
                        parentDiv.insertBefore(sp1, sp2.nextSibling);

                如果 sp2 没有下一个节点，则它肯定是最后一个节点，则 sp2.nextSibling 返回 null，且 sp1 被插入到子节点列表的最后面（即 sp2 后面）。
            
            */
            //  newChildren[i] 不是真正的DOM  需要创建新的dom 返回值是真实的DOM （newChildren[i].elm）
            parentElm.insertBefore(createElement(newChildren[i]), before);
            // parentElm.insertBefore(createElement(newChildren[i]), oldChildren[oldStartIndex]);
        }

    }
    console.log("有剩余成功");

}