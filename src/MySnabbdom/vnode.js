// 函数功能： 将传入的5个参数，组合成对象
export default function vnode(sel, data, children, text, elm) {
    const key = data.key;
    return {
        sel,
        data,
        children,
        text,
        elm,
        key
    }
}