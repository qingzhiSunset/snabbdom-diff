import h from './MySnabbdom/h'
import patch from './MySnabbdom/patch'

// const vnode = h('h1', {}, "snabbdom");
// const vnode = h('ul', {}, [
//     h('li', {}, "1"),
//     h('li', {}, "2"),
//     h('li', {}, "3"),
//     h('li', {}, [h('ol', {}, [
//             h('li', {}, h('div', {}, "哈哈哈")),
//             h('li', {}, h('div', {}, "哈哈哈")),
//             h('li', {}, h('div', {}, "哈哈哈"))
//         ])

//     ]),
// ]);
// const container = document.querySelector('.container');
// patch(container, vnode);

// console.log(container)


// 得到按钮和container
const btn = document.querySelector('.btn');
const container = document.querySelector('.container')



// const vnode1 = h('section', {}, [
//     h('p', { key: "A" }, 'A'),
//     h('p', { key: "B" }, 'B'),
//     h('p', { key: "C" }, 'C')
// ])

// // 点击按钮时将vnode1 变成vnode2
// // 创建虚拟节点
// const vnode2 = h('section', {}, [
//     h('p', { key: "A" }, 'AAA'),
//     h('p', { key: "B" }, 'BBB'),
//     h('p', { key: "M" }, 'M'),
//     h('p', { key: "D" }, 'N'),
//     h('p', { key: "C" }, 'CCC'),

// ])
// // 测试策略三：
// const vnode1 = h('section', {}, [
//     h('p', { key: "A" }, 'A'),
//     h('p', { key: "B" }, 'B'),
//     h('p', { key: "C" }, 'C'),
//     h('p', { key: "D" }, 'D'),
//     h('p', { key: "E" }, 'E'),
// ])

// // 点击按钮时将vnode1 变成vnode2
// // 创建虚拟节点
// const vnode2 = h('section', {}, [
//     h('p', { key: "E" }, 'EEE'),
//     h('p', { key: "D" }, 'DDD'),
//     h('p', { key: "C" }, 'CCCC'),
//     h('p', { key: "B" }, 'BBBB'),
//     h('p', { key: "A" }, 'AAAA'),

// ])

// // 新节点有剩余：
// const vnode1 = h('section', {}, [
//     h('p', { key: "A" }, 'A'),
//     h('p', { key: "B" }, 'B'),
//     h('p', { key: "C" }, 'C'),
//     h('p', { key: "D" }, 'D'),
//     h('p', { key: "E" }, 'E'),
// ])

// // 点击按钮时将vnode1 变成vnode2
// // 创建虚拟节点
// const vnode2 = h('section', {}, [
//     h('p', { key: "A" }, 'A'),
//     h('p', { key: "B" }, 'B'),
//     h('p', { key: "C" }, 'C'),
//     h('p', { key: "D" }, 'D'),
//     h('p', { key: "E" }, 'E'),
//     h('p', { key: "F" }, 'F'),
//     h('p', { key: "G" }, 'G'),

// ])


// 旧节点有剩余：
const vnode1 = h('section', {}, [
    h('p', { key: "A" }, 'A'),
    h('p', { key: "B" }, 'B'),
    h('p', { key: "Cw" }, 'Cw'),
    h('p', { key: "C1" }, 'C1'),
    h('p', { key: "D" }, 'D'),
    h('p', { key: "E1" }, 'E1'),
    h('p', { key: "R" }, 'R'),
    h('p', { key: "E2" }, 'E2'),
    h('p', { key: "C" }, 'C'),
    h('p', { key: "E3" }, 'E3'),
    h('p', { key: "E4" }, 'E4'),
])

// 点击按钮时将vnode1 变成vnode2
// 创建虚拟节点
const vnode2 = h('section', {}, [
        h('p', { key: "R" }, 'R'),
        h('p', { key: "E" }, 'E'),
        h('p', { key: "B" }, 'B'),
        h('p', { key: "Q" }, 'Q'),
        h('p', { key: "A" }, 'A'),
        h('p', { key: "D" }, 'D'),
        h('p', { key: "C" }, h('p', {}, [
            h('ul', {}, [
                h('li', {}, '1'),
                h('li', {}, '2'),
                h('li', {}, '3'),
                h('li', {}, [h('p', {}, "p1"),
                    h('p', {}, "p2"),
                    h('p', {}, "p3"),
                    h('p', {}, "p4")
                ])
            ])
        ])),
        h('p', { key: "L" }, 'L'),
        h('p', { key: "T" }, 'T'),
        h('p', { key: "F" }, 'F'),


    ])
    // const vnode2 = h('section', {}, [
    //     h('p', { key: "C" }, 'C'),


// ])

// patch 让虚拟节点上树
patch(container, vnode1);
btn.onclick = function() {
    patch(vnode1, vnode2);
}