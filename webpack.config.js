const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: resolve(__dirname, 'dist'),
        filename: "js/bundle.js",
        environment: {
            //不使用箭头函数
            arrowFunction: false
        },
    },
    mode: "development",
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
    ],
    //设置引用文件
    resolve: {
        extensions: ['.ts', '.js']
    },
    devServer: {
        contentBase: resolve(__dirname, 'www'),
        compress: false,
        port: 8080,
        open: true,
        // 开启HMR功能
        // 当修改了webpack配置，新配置要想生效，必须重启webpack服务
        hot: true
    },
    devtool: 'eval-source-map' //使用：直接加上这句话即可

}